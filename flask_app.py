# A very simple Flask Hello World app for you to get started with...

from flask import Flask, redirect, url_for, render_template, request
from flask_login import LoginManager, UserMixin,  current_user, login_user, login_required, logout_user
from werkzeug.security import generate_password_hash, check_password_hash
from pony.orm import Database, Required, Optional, db_session, select
import keygen
from forms import LoginForm
import random

triangle = {'rock': 'scissors', 'scissors': 'paper', 'paper': 'rock'}
aiscore = 0
playerscore = 0
playerchoice = ""
message = ""
winner = ""
rounds = -1
choices = ['rock', 'paper', 'scissors']
new_playerchoice = ""
tmpnew_playerchoice = ""
tie_counter = 0

app = Flask(__name__)
app.secret_key = keygen.generate()
login = LoginManager(app)
login.login_view = 'login'

db = Database()


class User(UserMixin, db.Entity):

    
    username = Required(str, unique=True)
    password_hash = Optional(str)
    points = Optional(int, default=0)
    

    @db_session
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    @db_session
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)


db.bind(provider='sqlite', filename='mydb', create_db=True)
db.generate_mapping(create_tables=True)


@login.user_loader
@db_session
def load_user(id):
    return User.get(id=int(id))


@app.route('/')
@login_required
@db_session
def index():
    if request.method == 'GET':
        users = list(select(t for t in User))
        return render_template('index.html', NAME=current_user.username, Players=users)

@app.route('/login', methods=['GET', 'POST'])
@db_session
def login():

    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()

    if form.validate_on_submit():
        user = User.get(username=form.username.data)

        if user is None or not user.check_password(form.password.data):
            return redirect(url_for('login'))

        login_user(user)  # remember=form.remember_me.data)
        return redirect(url_for('index'))

    return render_template('login.html', title='Sign In', form=form)


@app.route('/new_user', methods=['GET', 'POST'])
@db_session
def new_user_form():
    if request.method == 'GET':
        return render_template('newuserform.html')

    elif request.method == 'POST':
        data = request.form.to_dict()

        u = User(username=data['username'])
        u.set_password(data['password'])

        return redirect(url_for('index'))


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/')


@app.route('/index/RPS/', methods=['GET', 'POST'])
@login_required
@db_session
def rockpaperscissors():
    global playerchoice, tie_counter, triangle, aiscore, playerscore, message, winner, rounds, choices, new_playerchoice
    
    if request.method == 'GET':
        
        new_playerchoice = request.args.get("choise")
        new_aichoice = random.choice(choices)
        if rounds >= 0:
            if new_aichoice == new_playerchoice:
                message = 'TIE'
                tie_counter += 1
            elif triangle[new_playerchoice] == new_aichoice:
                message = 'You win!'
                playerscore += 1
            else:
                message = 'Alien wins'
                aiscore += 1

        rounds += 1

    if aiscore >= 3:
        winner = 'Alien wins the game'
        rounds = 0
        aiscore = 0
        playerscore = 0
        tie_counter = 0
        
    elif playerscore >= 3:
        winner = 'You win the game!'
        #query1 = select(p for p in User if current_user.username==p.username)
        #query1.points += 1
        #current_user.points = current_user.points + 1
        #points +=1
        rounds = 0
        aiscore = 0
        playerscore = 0
        tie_counter = 0
        
        

    else:
        winner = ''

    return render_template('rps.html', AI_POINTS=aiscore, YOUR_POINTS=playerscore, RANGU=rounds, Message=message, NAME=current_user.username,POINTS=current_user.points, Winner=winner, AI=new_aichoice, YOU=new_playerchoice,TIES=tie_counter )


if __name__ == '__main__':
    app.run(threaded=True, port=5000)


'''
    global round_number,winner
    if request.method == 'GET':
        return render_template('rps.html', NAME=current_user.username)
    

    elif request.method == 'POST':
        while player_wins < 3 and alien_wins < 3:
            alienchoice = random.choice(choices)
            playerchoice =  request.form.get('choise')
            
            round_number += 1
        if alien_wins > player_wins:
            winner = 'AI wins the game'

        elif  player_wins > alien_wins:
            winner='You win the game!'
        else:
            winner=''  
        return render_template('rps.html', AI_POINTS=alien_wins, YOUR_POINTS=player_wins,RANGU=round_number,Message=message, NAME=current_user.username, Winner=winner,AI=alienchoice, YOU=playerchoice, TIES= tie_count )

if __name__ == '__main__':
    app.run(threaded=True, port=5000)
        

'''
